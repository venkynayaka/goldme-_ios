//
//  AccountViewController.swift
//  GoldMe
//
//  Created by Zeenath on 07/07/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataArray = ["My Account","Wish List","Purchase History","Notifications","Track Order","Settings"]
    var imageArray = ["account","heart","history","notifications","track","notifications"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AccountViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0)
        {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "accountCell") as! AccountTableViewCell
            return cell
        }
        else{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "otherCell") as! AccountTableViewCell
            cell.dataTextLabel.text = dataArray[indexPath.row]
            cell.dataImageView.image = UIImage.init(named: imageArray[indexPath.row])
            return cell
        }
        
    }
}

extension AccountViewController : UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 1)
        {
            return 6
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.section == 1)
        {
            return 75
        }
        return 105
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 1){
            if(indexPath.row == 0){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyAccountViewController") as! MyAccountViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if(indexPath.row == 1){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WishListViewController") as! WishListViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        else if(indexPath.row == 3){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else  if(indexPath.row == 5){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

}

