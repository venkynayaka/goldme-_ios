//
//  AddToCartViewController.swift
//  GoldMe
//
//  Created by Zeenath on 23/06/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit
import Alamofire

class AddToCartViewController: UIViewController {

    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    var array = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getAllData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    func getAllData() {
        // _ = HKProgressHUD.show(addedToView: (self.tableView)!, animated: true)
        showActivity()
        Alamofire.request("https://simplycodinghub.com/jojo/list_of_category.php" , method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON {
            response in
            if response.result.isSuccess{
                DispatchQueue.main.async{
                    DispatchQueue.global(qos: .userInitiated).async {
                        
                        DispatchQueue.main.async {
                            //  _ = HKProgressHUD.hide(addedToView: self.tableView, animated: true)
                        }
                    }
                    print(response)
                    
                    let JSON = response.result.value as! [[String:Any]]
                    self.array = JSON
                    
                    if self.array.count > 0
                    {
                        self.hideActivity()
                        self.tableView.reloadData()
                    }
                    else{
                        //self.showNoData()
                    }
                    
                }
            }
            else{
                print("Error \(String(describing: response.result.error))")
                
            }
            
        }
    }
    
    func showActivity()
    {
        self.indicatorView.isHidden = false
        indicatorView.startAnimating()
    }
    
    func hideActivity()
    {
        self.indicatorView.stopAnimating()
        self.indicatorView.isHidden = true
    }
    
    
}

extension AddToCartViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0)
        {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cartCell") as! AddToCartTableViewCell
        
        cell.itemPriceLabel.text = array[indexPath.row]["item_price"] as? String
        cell.itemCodeLabel.text = array[indexPath.row]["item_code"] as? String
        cell.itemDiscountLabel.text = array[indexPath.row]["item_discount"] as? String
        cell.itemNameLabel.text = array[indexPath.row]["item_name"] as? String
        let url = URL(string: array[indexPath.row]["item_image"] as! String)
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {
                cell.itemImageView.image = UIImage(data: data!)
            }
        }
        return cell
        }
        else{
           let cell = self.tableView.dequeueReusableCell(withIdentifier: "priceCell") as! AddToCartTableViewCell
           return cell
        }
        
    }
}

extension AddToCartViewController : UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0)
        {
            return array.count
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.section == 0)
        {
            return 235
        }
        return 270
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

