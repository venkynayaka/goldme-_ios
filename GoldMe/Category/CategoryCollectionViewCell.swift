//
//  CategoryCollectionViewCell.swift
//  GoldMe
//
//  Created by Zeenath on 20/07/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemCodeLabel: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDiscountLabel: UILabel!
    
}
