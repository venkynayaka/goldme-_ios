//
//  CategoryViewController.swift
//  GoldMe
//
//  Created by Zeenath on 04/06/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit
import Alamofire


class CategoryViewController: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var filterTable: UITableView!
    
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    var array = [[String:Any]]()
    
    var filterTitleArray = ["Sort By","Gender","Type","Brand","Carat"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let gesture = UITapGestureRecognizer(target: self, action:   #selector(self.checkAction))
        popUpView.addGestureRecognizer(gesture)
        self.getAllData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        popUpView.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getAllData() {
       // _ = HKProgressHUD.show(addedToView: (self.tableView)!, animated: true)
        showActivity()
        Alamofire.request("https://simplycodinghub.com/jojo/list_of_category.php" , method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON {
            response in
            if response.result.isSuccess{
                DispatchQueue.main.async{
                    DispatchQueue.global(qos: .userInitiated).async {
                        
                        DispatchQueue.main.async {
                          //  _ = HKProgressHUD.hide(addedToView: self.tableView, animated: true)
                        }
                    }
                    print(response)
                    
                    let JSON = response.result.value as! [[String:Any]]
                    self.array = JSON
                    
                    if self.array.count > 0
                    {
                        self.hideActivity()
                        self.collectionView.reloadData()
                    }
                    else{
                        //self.showNoData()
                    }
                    
                }
            }
            else{
                print("Error \(String(describing: response.result.error))")
                
            }
            
        }
    }
    
    func showActivity()
    {
        self.indicatorView.isHidden = false
        indicatorView.startAnimating()
    }
    
    func hideActivity()
    {
        self.indicatorView.stopAnimating()
        self.indicatorView.isHidden = true
    }
    
    @IBAction func filterButtonClicked(_ sender: Any) {
        popUpView.isHidden = false
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension CategoryViewController : UICollectionViewDataSource {
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCollectionViewCell
    cell.itemCodeLabel.text = array[indexPath.item]["item_price"] as? String
    cell.itemDiscountLabel.text = array[indexPath.item]["item_discount"] as? String
    cell.itemNameLabel.text = array[indexPath.item]["item_name"] as? String
    let url = URL(string: array[indexPath.item]["item_image"] as! String)
    
    DispatchQueue.global().async {
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        DispatchQueue.main.async {
            cell.itemImageView.image = UIImage(data: data!)
        }
    }
    
        return cell
    }
    
}

extension CategoryViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
        
}

extension CategoryViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = filterTable.dequeueReusableCell(withIdentifier: "filterCell") as! FilterTableViewCell
        cell.filterTitleLabel.text = filterTitleArray[indexPath.row]
        return cell
    }
}

extension CategoryViewController : UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filterTitleArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}




