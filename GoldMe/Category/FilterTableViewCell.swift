//
//  FilterTableViewCell.swift
//  GoldMe
//
//  Created by Zeenath on 25/07/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var filterText: UITextField!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var filterTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
