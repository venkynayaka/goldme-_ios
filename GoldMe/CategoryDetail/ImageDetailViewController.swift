//
//  ImageDetailViewController.swift
//  GoldMe
//
//  Created by Zeenath on 22/06/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit
import Alamofire

class ImageDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var array = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getAllData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }

    func getAllData() {
        // _ = HKProgressHUD.show(addedToView: (self.tableView)!, animated: true)
        Alamofire.request("https://simplycodinghub.com/jojo/item_details.php" , method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON {
            response in
            if response.result.isSuccess{
                DispatchQueue.main.async{
                    DispatchQueue.global(qos: .userInitiated).async {
                        
                        DispatchQueue.main.async {
                            //  _ = HKProgressHUD.hide(addedToView: self.tableView, animated: true)
                        }
                    }
                    print(response)
                    
                    let JSON = response.result.value as! [String:Any]
                    self.array = JSON
                    
                    if self.array.count > 0
                    {
                        //self.hideNoData()
                        self.tableView.reloadData()
                    }
                    else{
                        //self.showNoData()
                    }
                    
                }
            }
            else{
                print("Error \(String(describing: response.result.error))")
                
            }
            
        }
    }
    

}

extension ImageDetailViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0)
        {
            let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "imageCell") as! ItemImageTableViewCell
            
            return cell
        }
        else if(indexPath.section == 1){
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "dataCell") as! ItemDataTableViewCell
            
            cell.itemNameLabel.text = array["item_name"] as? String
            cell.itemSubNameLabel.text = array["item_sub_name"] as? String
            cell.itemCodeLabel.text = array["item_code"] as? String
            cell.heightLabel.text = array["item_height"] as? String
            cell.widthLabel.text = array["item_width"] as? String
            cell.productWeightLabel.text = array["item_weight"] as? String
            cell.shapeLabel.text = array["item_shape"] as? String
            cell.colorLabel.text = array["item_code"] as? String
            cell.typeLabel.text = array["item_type"] as? String
            cell.chargeLabel.text = array["item_making_charges"] as? String
            cell.noOfDiamonandLabel.text = array["item_star"] as? String
            
            return cell
        }
        else if(indexPath.section == 2){
            let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "infoCell") as! ItemInfoTableViewCell
            return cell
        }
        else if(indexPath.section == 3){
            let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "trackCell") as! ItemTrackTableViewCell
            return cell
        }
        else if(indexPath.section == 4){
            let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "policyCell") as! ItemPaymentPolicyTableViewCell
            return cell
        }
        else if(indexPath.section == 5){
            let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "conditionsCell") as! ItemTermsConditionsTableViewCell
            return cell
        }
        else if(indexPath.section == 6){
            let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "collectionCell") as! ImageCollectionTableViewCell
            return cell
        }
        else {
            let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "reviewCell") as! ItemReviewTableViewCell
            return cell
        }
    }
}

extension ImageDetailViewController : UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return 1
   }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.section == 0)
        {
            return 230
        }
        else if(indexPath.section == 1){
            return 350
        }
        else if(indexPath.section == 2)
        {
           return 185
        }
        else if(indexPath.section == 3)
        {
            return 231
        }
        else if(indexPath.section == 6)
        {
            return 225
        }
       else
        {
            return 160
        }
    }
    
}


