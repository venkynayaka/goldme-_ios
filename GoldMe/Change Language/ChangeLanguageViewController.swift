//
//  ChangeLanguageViewController.swift
//  GoldMe
//
//  Created by Zeenath on 27/07/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class ChangeLanguageViewController: UIViewController {

    @IBOutlet weak var arabicButton: UIButton!
    @IBOutlet weak var englishButton: UIButton!
    @IBOutlet weak var hindiButton: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func languageChangeButtonClick(_ sender: UIButton) {
        if sender.tag == 1
        {
           NotificationCenter.default.post(name: NSNotification.Name("changeArabic"), object: nil)
        }else{
           NotificationCenter.default.post(name: NSNotification.Name("changeEnglish"), object: nil)
        }
        
        sender.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .selected)
        sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        sender.setBackgroundColor(color: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), forState: .normal)
        sender.setBackgroundColor(color: #colorLiteral(red: 0.9974876046, green: 0.8034962416, blue: 0.06895182282, alpha: 1), forState: .selected)
        
        let buttonArray = [arabicButton,englishButton,hindiButton]
        
        buttonArray.forEach{
            $0?.isSelected = false
            sender.setBackgroundColor(color: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), forState: .normal)
            sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        }
        sender.isSelected = true
    }
    

}

extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        self.clipsToBounds = true  // add this to maintain corner radius
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: forState)
        }
    }
}
