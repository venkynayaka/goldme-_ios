//
//  LoginViewController.swift
//  GoldMe
//
//  Created by mohammad shoaib ahmed on 30/05/19.
//  Copyright © 2019 BlissBling for operation of websiteandweb. All rights reserved.
//

import UIKit
import CoreLocation
import Social
import FacebookLogin
import FacebookCore
import FBSDKLoginKit


class LoginViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var facebook: UIButton!
    @IBOutlet weak var instagram: UIButton!
    @IBOutlet weak var orLabel: UIView!
    @IBOutlet weak var orLabeltext: UILabel!
    
    var userType: String?
    var loginType: String?
    let locationManager = CLLocationManager()
    
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var et_email_phone: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title="Create Account"
       // self.navigationController?.navigationBar.isHidden = true
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        
        // Do any additional setup after loading the view.
        print(userType!)
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        UserDefaults.standard.set(locValue.latitude, forKey: "latitude")
        UserDefaults.standard.set(locValue.longitude, forKey: "longitude")
    }
    
    @IBAction func continueClicked(_ sender: UIButton) {
        loginType = "manual"
//        self.performSegue(withIdentifier: "loginDetails", sender: self)
        self.performSegue(withIdentifier: "my_account", sender: self)
    }
    
    @IBAction func instagramClicked(_ sender: UIButton) {
        loginType = "instagram"
        self.performSegue(withIdentifier: "Instalogin", sender: self)
    }
    @IBAction func facebookClicked(_ sender: UIButton) {
        loginType = "facebook"
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, _, _):
                print(grantedPermissions)
                print("Logged in!")
                let connection = GraphRequestConnection()
                connection.add(GraphRequest(graphPath: "/me", parameters: ["fields":"email"])) { httpResponse, result, error   in
                    if error != nil {
                        NSLog(error.debugDescription)
                        return
                    }
                    
                    // Handle vars
                    if let result = result as? [String:String],
                        let email: String = result["email"],
                        let fbId: String = result["id"] {
                     }
                    
                }
                connection.start()
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "loginDetails") {
            let detailVC = segue.destination as! LoginDetailsViewController
            detailVC.email = et_email_phone.text
            detailVC.loginType = loginType!
        }
        else if(segue.identifier == "my_account")
        {
           let detailVC = segue.destination as! MyAccountViewController
           detailVC.email = et_email_phone.text
           detailVC.loginType = loginType!
        }
    }

}
