//
//  ViewController.swift
//  GoldMe
//
//  Created by mohammad shoaib ahmed on 29/05/19.
//  Copyright © 2019 BlissBling for operation of websiteandweb. All rights reserved.
//

import UIKit
import CLabsImageSlider

class loginTypePage: UIViewController {
    
    @IBOutlet weak var seller: UIButton!
    @IBOutlet weak var buyer: UIButton!
    
    @IBOutlet weak var imageSlider: CLabsImageSlider!
    @IBOutlet weak var pageControl: UIPageControl!
    
    let localImages =   ["gm1","gm2","gm3","gm4"]
    
    var userType: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        imageSlider.sliderDelegate = self
        imageSlider.setUpView(imageSource: .Local(imageArray: localImages),slideType: .Automatic(timeIntervalinSeconds: 5),isArrowBtnEnabled: false)
        self.view.bringSubviewToFront(pageControl)
       // self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    @IBAction func buyerClicked(_ sender: UIButton) {
        
        userType = "Buyer"
        self.performSegue(withIdentifier: "loginSegue", sender: self)
    }
    
    @IBAction func sellerClicked(_ sender: UIButton) {
       
        userType = "Seller"
       // self.performSegue(withIdentifier: "loginSegue", sender: self)
    }
   
    
  
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "loginSegue") {
            let loginVC = segue.destination as! LoginViewController
            loginVC.userType = userType
        }
    }
}

extension loginTypePage: imageSliderDelegate{
    func didMovedToIndex(index: Int) {
        pageControl.currentPage = index
        print("did moved at Index : ",index)
    }
}
