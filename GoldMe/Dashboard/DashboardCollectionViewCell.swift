//
//  DashboardCollectionViewCell.swift
//  GoldMe
//
//  Created by Zeenath on 09/06/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class DashboardCollectionViewCell: UICollectionViewCell {
   @IBOutlet weak var itemImage: UIImageView!
}
