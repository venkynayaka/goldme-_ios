//
//  DashboardDataTableViewCell.swift
//  GoldMe
//
//  Created by Zeenath on 02/06/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class DashboardDataTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var itemImage: UIImageView!
    var collectArray = ["gmb1","gmb2","gmb3","gmb4"]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension DashboardDataTableViewCell : UICollectionViewDataSource {
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! DashboardCollectionViewCell
        cell.itemImage.image = UIImage.init(named: collectArray[indexPath.row])
        return cell
    }
    
}

extension DashboardDataTableViewCell : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}

