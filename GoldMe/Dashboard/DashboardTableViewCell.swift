//
//  DashboardTableViewCell.swift
//  GoldMe
//
//  Created by Zeenath on 01/06/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit
import CLabsImageSlider

class DashboardTableViewCell: UITableViewCell,imageSliderDelegate  {
   
    @IBOutlet weak var imageSlider: CLabsImageSlider!
    @IBOutlet weak var pageControl: UIPageControl!
    
    let localImages =   ["gm1","gm2","gm3","gm4"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageSlider.sliderDelegate   =   self
        imageSlider.setUpView(imageSource: .Local(imageArray: localImages),slideType: .Automatic(timeIntervalinSeconds: 5),isArrowBtnEnabled: false)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func didMovedToIndex(index: Int) {
        pageControl.currentPage = index
        print("did moved at Index : ",index)
    }
    

    

}


