//
//  ViewController.swift
//  GoldMe
//
//  Created by Zeenath on 01/06/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var locationButton: UIButton!
    
    var array = ["gm1","gm2","gm3","gm4"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
       getAddressFromLatLon()
      
      NotificationCenter.default.addObserver(self, selector: #selector(changeToArabic), name: NSNotification.Name.init(rawValue: "changeArabic"), object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(changeToEnglish), name: NSNotification.Name.init(rawValue: "changeEnglish"), object: nil)
        
    }
    
    @objc func changeToArabic(){
       self.tabBarController!.tabBar.items?[0].title = "الصفحة الرئيسية"
       self.tabBarController!.tabBar.items?[1].title = "عربة التسوق"
        self.tabBarController!.tabBar.items?[2].title = "الحساب"
    }
    
    @objc func changeToEnglish(){
        self.tabBarController!.tabBar.items?[0].title = "Home"
        self.tabBarController!.tabBar.items?[1].title =  "Cart"
        self.tabBarController!.tabBar.items?[2].title = "Account"
    }
    
    func getAddressFromLatLon()  {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = UserDefaults.standard.value(forKey: "latitude")! as! Double
        //21.228124
        let lon: Double = UserDefaults.standard.value(forKey: "longitude")! as! Double
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    
                    
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country!
                    }
//                    if pm.postalCode != nil {
//                        addressString = addressString + pm.postalCode! + " "
//                    }
                    
                    
                    print("My Address izzz",addressString)
                    self.locationButton.setTitle(addressString, for: .normal)
                    
                }
        })
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @objc func addTapped(sender : UIBarButtonItem) {
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension ViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0)
        {
            let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "dashboardCell") as! DashboardTableViewCell
            
            return cell
        }
        else if(indexPath.section == 1){
            let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "collectionCell") as! DashboardDataTableViewCell
            return cell
        }
        else if(indexPath.section == 2){
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "dataCell") as! DashboardDataTableViewCell
            cell.itemImage.image = UIImage.init(named: array[indexPath.row])
            return cell
        }
        else {
            let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "collectionCell") as! DashboardDataTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section \(section)"
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! CustomHeaderCell
        switch (section) {
        case 0:
            headerCell.headerLabel.text = "GoldME Best Offers"
     
        case 1:
            headerCell.headerLabel.text = "GoldME Best Offers"
        
        case 2:
            headerCell.headerLabel.text = "All Category"
        
        default:
            headerCell.headerLabel.text = "Why GoldMe"
        }
        
        return headerCell
    }
    
}

extension ViewController : UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(section == 2)
        {
            return 4
        }
       return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.section == 0)
        {
            return 285
        }
        else if(indexPath.section == 2){
            return 170
        }
        else{
            return 130
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if(section == 0)
        {
           return 0
        }
        return 50
    }
    
    
}

