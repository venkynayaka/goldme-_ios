//
//  Constant.swift
//  InstagramLogin-Swift
//
//  Created by Aman Aggarwal on 2/7/17.
//  Copyright © 2017 ClickApps. All rights reserved.
//

import Foundation

struct INSTAGRAM_IDS {
    
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    
    static let INSTAGRAM_CLIENT_ID  = "28a855073546471b9e06439de1d37639"
    
    static let INSTAGRAM_CLIENTSERCRET = "49325236774a4705a7422494592fd45e"
    
    static let INSTAGRAM_REDIRECT_URI = "http://www.simplycodinghub.com"
    
    static let INSTAGRAM_ACCESS_TOKEN =  "access_token"
    
    static let INSTAGRAM_SCOPE = "likes+comments+relationships"
    
}

