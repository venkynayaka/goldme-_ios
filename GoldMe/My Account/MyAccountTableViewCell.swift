//
//  MyAccountTableViewCell.swift
//  GoldMe
//
//  Created by Zeenath on 27/07/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class MyAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var dataText: UITextField!
    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var femaleRadioButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
