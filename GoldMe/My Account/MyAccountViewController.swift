//
//  MyAccountViewController.swift
//  GoldMe
//
//  Created by Zeenath on 27/07/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class MyAccountViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var contentArray = ["","",""]
    var placeholderArray = ["Name","Email","Phone Number"]
    var email: String?
    var loginType: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if(email?.count ?? 0 > 0)
        {
            contentArray = ["","",email] as! [String]
            tableView.reloadData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func radioButtonClick(_ sender: UIButton) {
        let cell00 = tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as! MyAccountTableViewCell
        if sender.tag == 1
        {
            
        }else{
            
        }
        
        sender.setImage(UIImage(named: "radioOn"), for: .selected)
        sender.setImage(UIImage(named: "radioOff"), for: .normal)
        
        let buttonArray = [cell00.maleRadioButton,cell00.femaleRadioButton]
        
        buttonArray.forEach{
            
            $0?.isSelected = false
            $0?.setImage(UIImage(named: "radioOff"), for: .normal)
        }
        sender.isSelected = true
        
    }
    
    
    
    @IBAction func submitButtonClick(_ sender: Any) {
        let myDict = ["login_type":loginType,"user_phone":email,"social_id":"","user_name":contentArray[0],"user_email":contentArray[1]] as! [String : String]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPViewController") as! VerifyOTPViewController
        vc.email = contentArray[2]
        vc.dict = myDict
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MyAccountViewController : UITextFieldDelegate
{
    private func textFieldDidEndEditing(textField: UITextField) {
        if(textField.tag == 0){
            contentArray[0] = textField.text!
            
        }
        else{
            contentArray[1] = textField.text!
            
        }
        
    }
}

extension MyAccountViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0)
        {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "dataCell") as! MyAccountTableViewCell
            cell.dataText.text = contentArray[indexPath.row]
            cell.dataText.tag = indexPath.row
            cell.dataText.delegate = self
            
            if(indexPath.row == 2)
            {
                cell.dataText.isUserInteractionEnabled = false
            }
            else{
                cell.dataText.isUserInteractionEnabled = true
            }
            cell.dataText.placeholder = placeholderArray[indexPath.row]
            
            return cell
        }
        else if(indexPath.section == 1)
        {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "genderCell") as! MyAccountTableViewCell
            return cell
        }
        else{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "saveCell") as! MyAccountTableViewCell
            return cell
        }
    }
}

extension MyAccountViewController : UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0)
        {
            return 3
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.section == 0)
        {
            return 75
        }
        else if(indexPath.section == 1)
        {
            return 75
        }
        else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}


