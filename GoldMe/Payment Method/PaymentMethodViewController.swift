//
//  PaymentMethodViewController.swift
//  GoldMe
//
//  Created by Zeenath on 28/07/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class PaymentMethodViewController: UIViewController {
    
    var dataArray = ["Visa","PayPal","Master Card"]
    var imageArray = ["visa","paypal","masterCard"]

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PaymentMethodViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0)
        {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "accountCell") as! PaymentMethodTableViewCell
            return cell
        }
        else{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "modeCell") as! PaymentMethodTableViewCell
            cell.modeLabel.text = dataArray[indexPath.row]
            cell.modeImageView.image = UIImage.init(named: imageArray[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0){
            return "BANK ACCOUNTS"
            
        }
        else{return "PAYMENT MODE"}
        
    }
    
    
}

extension PaymentMethodViewController : UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 1)
        {
            return dataArray.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:50))
        
        let headerlabel = UILabel(frame: CGRect(x:0, y:0, width:headerView.frame.size.width, height:50))
        headerlabel.font = UIFont(name: "System", size: headerlabel.font.pointSize)
        if(section == 0)
        {
            headerlabel.text = "BANK ACCOUNTS"}
        else{
            headerlabel.text = "PAYMENT MODE"
            
        }
        
        headerlabel.textAlignment = .center
        headerlabel.textColor = #colorLiteral(red: 0.9974876046, green: 0.8034962416, blue: 0.06895182282, alpha: 1)
        headerView.addSubview(headerlabel)
        headerView.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
            return 50
    }
    
}


