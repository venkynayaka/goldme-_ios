//
//  SellerProfileViewController.swift
//  GoldMe
//
//  Created by Zeenath on 27/07/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class SellerProfileViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dataArray = ["My Order","Settings","Help and Support"]
    var imageArray = ["account","settings","history"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SellerProfileViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0)
        {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "accountCell") as! SellerProfileTableViewCell
            return cell
        }
        else{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "otherCell") as! SellerProfileTableViewCell
            cell.dataTextLabel.text = dataArray[indexPath.row]
            cell.dataImageView.image = UIImage.init(named: imageArray[indexPath.row])
            return cell
        }
        
    }
}

extension SellerProfileViewController : UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 1)
        {
            return 3
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.section == 1)
        {
            return 75
        }
        return 105
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 1){
              if(indexPath.row == 1){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
                //self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
}


