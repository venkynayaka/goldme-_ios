//
//  SellerDetailViewController.swift
//  GoldMe
//
//  Created by Zeenath on 27/07/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class SellerDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var placeholderArray = ["Your Name","Complany Name","CIN Number","Mobile Number"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SellerDetailViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "dataCell") as! SellerDetailTableViewCell
            cell.detailTextView.placeholder = placeholderArray[indexPath.row]
            return cell
        }
        else{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "nextCell") as! SellerDetailTableViewCell
            
            return cell
        }
        
    }
}

extension SellerDetailViewController : UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0)
        {
           return 4
        }
        else{
        return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.section == 0)
        {
            return 60
        }
        else{
            return 87
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}




