//
//  SettingsTableViewCell.swift
//  GoldMe
//
//  Created by Zeenath on 21/07/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var dataTextLabel: UILabel!
    @IBOutlet weak var dataImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
