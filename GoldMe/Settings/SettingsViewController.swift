//
//  SettingsViewController.swift
//  GoldMe
//
//  Created by Zeenath on 21/07/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dataArray = ["Change Language","Manage Address","Share","About Us","Privacy Policy","Logout","App Version"]
    var imageArray = ["language","manageaddress","share","aboutus","privacy","logout",""]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension SettingsViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsTableViewCell
        cell.dataTextLabel.text = dataArray[indexPath.row]
        cell.dataImageView.image = UIImage.init(named: imageArray[indexPath.row])
        return cell
    }
}

extension SettingsViewController : UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 0)
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangeLanguageViewController") as! ChangeLanguageViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(indexPath.row == 5){
            Utilities.sharedInstance.showAlert(title: "Logout", message: "Are you sure want to logout?", forController: self, completion: {_ in
                UserDefaults.standard.set(false, forKey: "UserLoggedIn")
                let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let rootViewController:UIViewController = storyboard.instantiateViewController(withIdentifier: "loginTypePage") as UIViewController
                let nav = UINavigationController(rootViewController: rootViewController)
                nav.navigationBar.tintColor = #colorLiteral(red: 0.9731351733, green: 0.7480360866, blue: 0, alpha: 1)
                nav.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                appDelegate.window?.rootViewController = nav
                self.dismiss(animated: true, completion: nil) })
        }
    }
    
}



