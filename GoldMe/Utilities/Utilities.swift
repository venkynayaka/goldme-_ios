//
//  Utilities.swift
//  D3P
//
//  Created by ceazeles on 23/07/18.
//  Copyright © 2018 ceazeles. All rights reserved.
//

import UIKit


class Utilities: NSObject {
    static let sharedInstance = Utilities()
    
    func showAlert(title:String,message:String,forController:UIViewController,completion: @escaping (_ callBack:Bool) -> ()) {
     
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            completion(true)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        alert.view.tintColor = UIColor.init(red: 225.0/255.0, green: 49.0/255.0, blue: 91/255.0, alpha: 1.0)
        forController.present(alert, animated: true)
        
    }
    
    func displayAlert(title:String,message:String,forController:UIViewController,completion: @escaping (_ callBack:Bool) -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            completion(true)
        }))

        alert.view.tintColor = UIColor.init(red: 225.0/255.0, green: 49.0/255.0, blue: 91/255.0, alpha: 1.0)
        forController.present(alert, animated: true)
    }
    
    func validateEmail(emailId:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailId)
    }
    
    func validateMobileNumber(mobile:String) -> Bool {
        let mobileRegEx = "[0-9]{10,15}$"
        
        let mobileTest = NSPredicate(format:"SELF MATCHES %@", mobileRegEx)
        return mobileTest.evaluate(with: mobile)
    }
    
    func showHUD(view:UIView,message:String) {
//        let hud = HKProgressHUD.show(addedToView: view, animated: true)
//        
//        // Set the Text mode
//        hud.mode = .text
//        hud.label?.text = NSLocalizedString(message, comment: "hud message title")
//        // Move to bottom center.
//        hud.offset = CGPoint(x: 0, y: view.frame.height/2)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
//            hud.removeFromSuperview()
//        }
    }
}
