//
//  VerifyOTPViewController.swift
//  GoldMe
//
//  Created by Zeenath on 27/07/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit
import Alamofire

class VerifyOTPViewController: UIViewController {
    
    var email: String?
    var loginType: String?
    var seconds = 60 //This variable will hold a starting value of seconds. It could be any amount above 0.
    var timer = Timer()
    var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
    var dict = [String:Any]()
   
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       // self.navigationController?.navigationBar.isHidden = true
        getOTP()
        self.messageLabel.text = "OTP sent to " + email!
        runTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        if(seconds > 0){
        seconds -= 1     //This will decrement(count down)the seconds.
        timerLabel.text = "Time Left 0:" + "\(seconds)"
        }
        else{
           timer.invalidate()
        }
    }
    
    
    func getOTP() {
        
        Alamofire.request("https://simplycodinghub.com/jojo/emp_reg.php", method: .post, parameters: dict, encoding: JSONEncoding.default).responseJSON {
            response in
            if response.result.isSuccess{
                DispatchQueue.main.async{
                   
                    print(response)
                    
                }
                
            }else{
                print("Error \(String(describing: response.result.error))")
                
            }
        }
        
    }
}
