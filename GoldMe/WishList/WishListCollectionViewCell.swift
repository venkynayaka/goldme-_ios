//
//  WishListCollectionViewCell.swift
//  GoldMe
//
//  Created by Zeenath on 01/08/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit

class WishListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemCodeLabel: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    
}
