//
//  WishListViewController.swift
//  GoldMe
//
//  Created by Zeenath on 01/08/19.
//  Copyright © 2019 Zeenath. All rights reserved.
//

import UIKit
import Alamofire

class WishListViewController: UIViewController {

    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
   
    var array = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getAllData()
    }
    
    func getAllData() {
        // _ = HKProgressHUD.show(addedToView: (self.tableView)!, animated: true)
       
        showActivity()
        Alamofire.request("https://simplycodinghub.com/jojo/user_favourite.php" , method: .get, parameters: nil, encoding: JSONEncoding.default).responseJSON {
            response in
            if response.result.isSuccess{
                DispatchQueue.main.async{
                    DispatchQueue.global(qos: .userInitiated).async {
                        
                        DispatchQueue.main.async {
                            //  _ = HKProgressHUD.hide(addedToView: self.tableView, animated: true)
                        }
                    }
                    print(response)
                    
                    let JSON = response.result.value as! [[String:Any]]
                    self.array = JSON
                    
                    if self.array.count > 0
                    {
                       self.hideActivity()
                        self.collectionView.reloadData()
                    }
                    else{
                        //self.showNoData()
                    }
                    
                }
            }
            else{
                print("Error \(String(describing: response.result.error))")
                
            }
            
        }
    }
    
    func showActivity()
    {
        self.indicatorView.isHidden = false
        indicatorView.startAnimating()
    }
    
    func hideActivity()
    {
        self.indicatorView.stopAnimating()
        self.indicatorView.isHidden = true
    }
    

}

extension WishListViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! WishListCollectionViewCell
        cell.itemCodeLabel.text = array[indexPath.item]["item_price"] as? String
        cell.itemNameLabel.text = array[indexPath.item]["item_name"] as? String
        let url = URL(string: array[indexPath.item]["item_image"] as! String)
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {
                cell.itemImageView.image = UIImage(data: data!)
            }
        }
        
        return cell
    }
    
}

extension WishListViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}

